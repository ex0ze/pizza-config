/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "basic_config.hpp"

namespace pizza
{

class balancer : public basic_config {
public:
    const std::string& broker_addr() const noexcept { return broker_addr_; }
    void set_broker_addr(std::string broker_addr) noexcept { broker_addr_ = std::move(broker_addr); }

    const std::string& addr() const noexcept { return addr_; }
    void set_addr(std::string addr) noexcept { addr_ = std::move(addr); }

    const std::string& service_name() const noexcept { return service_name_; }
    void set_service_name(std::string service_name) noexcept { service_name_ = std::move(service_name); }

    virtual void to_json(rapidjson::Document& doc) const override {
        basic_config::to_json(doc);
        json::add("balancer_addr", addr_.c_str(), doc, doc.GetAllocator());
        json::add("broker_addr", broker_addr_.c_str(), doc, doc.GetAllocator());
        json::add("service_name", service_name_.c_str(), doc, doc.GetAllocator());
    }

    virtual void from_json(rapidjson::Document& doc) override {
        basic_config::from_json(doc);
        addr_ = json::get<const char*>("balancer_addr", doc);
        broker_addr_ = json::get<const char*>("broker_addr", doc);
        service_name_ = json::get<const char*>("service_name", doc);
    }

    DECLARE_KEY_GETTER(broker_public_key);
    DECLARE_KEY_GETTER(balancer_private_key);
    DECLARE_KEY_GETTER(balancer_public_key);
    
private:
    std::string broker_addr_;
    std::string addr_;
    std::string service_name_;
};

}
