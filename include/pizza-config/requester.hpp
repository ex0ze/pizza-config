/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "basic_config.hpp"

namespace pizza
{

class requester : public basic_config {
public:
    class nested_broker {
    public:
        template <typename Alloc>
        void to_json(rapidjson::Value& json_object, Alloc& allocator) const {
            json::add("name", name_.c_str(), json_object, allocator);
            json::add("addr", addr_.c_str(), json_object, allocator);
            if (encryption_) {
                json::add("encryption", encryption_, json_object, allocator);
                json::add("public_key", public_key_.c_str(), json_object, allocator);
            }
        }

        void from_json(const rapidjson::Value& json_object) {
            name_ = json::get<const char*>("name", json_object);
            addr_ = json::get<const char*>("addr", json_object);
            if (auto v = json::maybe_get<bool>("encryption", json_object); v) {
                encryption_ = *v;
                if (encryption_)
                    public_key_ = json::get<const char*>("public_key", json_object);
            }
            else encryption_ = false;
        }

        constexpr const std::string& name() const noexcept { return name_; }
        constexpr std::string& name() noexcept { return name_; }
        void set_name(std::string name) noexcept { name_ = std::move(name); }

        constexpr const std::string& addr() const noexcept { return addr_; }
        constexpr std::string& addr() noexcept { return addr_; }
        void set_addr(std::string addr) noexcept { addr_ = std::move(addr); }

        constexpr bool encryption() const noexcept { return encryption_; }

        constexpr const std::string& public_key() const noexcept { return public_key_; }
        constexpr std::string& public_key() noexcept { return public_key_; }
        void set_public_key(std::string public_key) noexcept {
            encryption_ = true;
            public_key_ = std::move(public_key);
        }

    private:
        std::string name_;
        std::string addr_;
        bool encryption_ = false;
        std::string public_key_;
    };

    class nested_bus {
    public:
        template <typename Alloc>
        void to_json(rapidjson::Value& json_object, Alloc& allocator) const {
            json::add("name", name_.c_str(), json_object, allocator);
            json::add("pub_addr", pub_addr_.c_str(), json_object, allocator);
            json::add("sub_addr", sub_addr_.c_str(), json_object, allocator);
            if (encryption_) {
                json::add("encryption", encryption_, json_object, allocator);
                json::add("pub_public_key", pub_public_key_.c_str(), json_object, allocator);
                json::add("sub_public_key", sub_public_key_.c_str(), json_object, allocator);
            }
        }

        void from_json(const rapidjson::Value& json_object) {
            name_ = json::get<const char*>("name", json_object);
            pub_addr_ = json::get<const char*>("pub_addr", json_object);
            sub_addr_ = json::get<const char*>("sub_addr", json_object);
            if (auto v = json::maybe_get<bool>("encryption", json_object); v) {
                encryption_ = *v;
                if (encryption_) {
                    pub_public_key_ = json::get<const char*>("pub_public_key", json_object);
                    sub_public_key_ = json::get<const char*>("sub_public_key", json_object); 
                }
                
            }
            else encryption_ = false;
        }

        constexpr const std::string& name() const noexcept { return name_; }
        constexpr std::string& name() noexcept { return name_; }
        void set_name(std::string name) noexcept { name_ = std::move(name); }

        constexpr const std::string& pub_addr() const noexcept { return pub_addr_; }
        constexpr std::string& pub_addr() noexcept { return pub_addr_; }
        void set_pub_addr(std::string pub_addr) noexcept { pub_addr_ = std::move(pub_addr); }

        constexpr const std::string& sub_addr() const noexcept { return sub_addr_; }
        constexpr std::string& sub_addr() noexcept { return sub_addr_; }
        void set_sub_addr(std::string sub_addr) noexcept { sub_addr_ = std::move(sub_addr); }

        constexpr bool encryption() const noexcept { return encryption_; }

        constexpr const std::string& pub_public_key() const noexcept { return pub_public_key_; }
        constexpr std::string& pub_public_key() noexcept { return pub_public_key_; }
        void set_pub_public_key(std::string pub_public_key) noexcept {
            encryption_ = true;
            pub_public_key_ = std::move(pub_public_key);
        }

        constexpr const std::string& sub_public_key() const noexcept { return sub_public_key_; }
        constexpr std::string& sub_public_key() noexcept { return sub_public_key_; }
        void set_sub_public_key(std::string sub_public_key) noexcept {
            encryption_ = true;
            sub_public_key_ = std::move(sub_public_key);
        }

    private:
        std::string name_;
        std::string pub_addr_;
        std::string sub_addr_;
        bool encryption_ = false;
        std::string pub_public_key_;
        std::string sub_public_key_;
    };

    using brokers_map = std::unordered_map<std::string, nested_broker>;
    using buses_map = std::unordered_map<std::string, nested_bus>;

    virtual void to_json(rapidjson::Document& doc) const override {
        basic_config::to_json(doc);
        brokers_to_json(doc);
        buses_to_json(doc);
    }

    virtual void from_json(rapidjson::Document& doc) override {
        basic_config::from_json(doc);
        brokers_from_json(doc);
        buses_from_json(doc);
    }

    constexpr const brokers_map& nested_brokers() const noexcept { return nested_brokers_; }
    constexpr brokers_map& nested_brokers() noexcept { return nested_brokers_; }

    constexpr const buses_map& nested_buses() const noexcept { return nested_buses_; }
    constexpr buses_map& nested_buses() noexcept { return nested_buses_; }

private:
    void brokers_to_json(rapidjson::Document& doc) const {
        rapidjson::Value key, arr;
        key.SetString("broker_list");
        arr.SetArray();
        for (const auto& [name, cfg] : nested_brokers_) {
            rapidjson::Value obj;
            obj.SetObject();
            cfg.to_json(obj, doc.GetAllocator());
            arr.GetArray().PushBack(std::move(obj), doc.GetAllocator());
        }
        doc.AddMember(std::move(key), std::move(arr), doc.GetAllocator());
    }

    void brokers_from_json(const rapidjson::Document& doc) {
        using ex = config_exception;
        if (!doc.HasMember("broker_list")) return;
        if (!doc["broker_list"].IsArray())
            throw ex(ex::code::member_of_wrong_type, "'broker_list' must be array type");
        for (const auto& obj : doc["broker_list"].GetArray()) {
            if (!obj.IsObject())
                throw ex(ex::code::member_of_wrong_type, "values in 'broker_list' must be object type");
            nested_broker b;
            b.from_json(obj);
            std::string name = b.name();
            nested_brokers_.emplace(name, std::move(b));
        }
    }

    void buses_to_json(rapidjson::Document& doc) const {
        rapidjson::Value key, arr;
        key.SetString("bus_list");
        arr.SetArray();
        for (const auto& [name, cfg] : nested_buses_) {
            rapidjson::Value obj;
            obj.SetObject();
            cfg.to_json(obj, doc.GetAllocator());
            arr.GetArray().PushBack(std::move(obj), doc.GetAllocator());
        }
        doc.AddMember(std::move(key), std::move(arr), doc.GetAllocator());
    }

    void buses_from_json(const rapidjson::Document& doc) {
        using ex = config_exception;
        if (!doc.HasMember("bus_list")) return;
        if (!doc["bus_list"].IsArray())
            throw ex(ex::code::member_of_wrong_type, "'bus_list' must be array type");
        for (const auto& obj : doc["bus_list"].GetArray()) {
            if (!obj.IsObject())
                throw ex(ex::code::member_of_wrong_type, "values in 'bus_list' must be object type");
            nested_bus b;
            b.from_json(obj);
            std::string name = b.name();
            nested_buses_.emplace(name, std::move(b));
        }
    }

    brokers_map nested_brokers_;
    buses_map nested_buses_;
};

}
