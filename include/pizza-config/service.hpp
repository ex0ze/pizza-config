/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>

#include "basic_config.hpp"


namespace pizza
{

class service : public basic_config {
public:
    enum bus_status : int {
        no_bus = 0,
        has_publishers_addr = 1,
        has_subscribers_addr = 2,
        has_all_addr = 3
    };
    const std::string& broker_addr() const noexcept { return broker_addr_; }
    void set_broker_addr(std::string broker_addr) noexcept { broker_addr_ = std::move(broker_addr); }

    const std::string& balancer_addr() const noexcept { return balancer_addr_; }
    void set_balancer_addr(std::string balancer_addr) noexcept {
        balancer_addr_ = std::move(balancer_addr);
        has_balancer_ = true;
    }
    constexpr bool has_balancer() const noexcept { return has_balancer_; }

    const std::string& bus_subscribers_addr() const noexcept { return bus_subscribers_addr_; }
    void set_bus_subscribers_addr(std::string bus_subscribers_addr) noexcept {
        bus_subscribers_addr_ = std::move(bus_subscribers_addr);
        bus_status_ |= bus_status::has_subscribers_addr;
    }
    const std::string& bus_publishers_addr() const noexcept { return bus_publishers_addr_; }
    void set_bus_publishers_addr(std::string bus_publishers_addr) noexcept {
        bus_publishers_addr_ = std::move(bus_publishers_addr);
        bus_status_ |= bus_status::has_publishers_addr;
    }
    constexpr bool has_bus() const noexcept { return bus_status_ == bus_status::has_all_addr; }

    const std::vector<std::string>& cl_args() const noexcept { return cl_args_; }
    std::vector<std::string>& cl_args() noexcept { return cl_args_; }

    constexpr std::size_t threads() const noexcept { return threads_; }
    constexpr void set_threads(std::size_t threads) noexcept { threads_ = threads; }

    virtual void to_json(rapidjson::Document& doc) const override {
        basic_config::to_json(doc);
        json::add("broker_addr", broker_addr_.c_str(), doc, doc.GetAllocator());
        if (has_balancer_)
            json::add("balancer_addr", balancer_addr_.c_str(), doc, doc.GetAllocator());
        if (has_bus()) {
            json::add("bus_publishers_addr", bus_publishers_addr_.c_str(), doc, doc.GetAllocator());
            json::add("bus_subscribers_addr", bus_subscribers_addr_.c_str(), doc, doc.GetAllocator());

        }
        if (!cl_args_.empty()) add_cl_args(cl_args_, doc);
        if (threads_ > 1)
            json::add("threads", threads_, doc, doc.GetAllocator());
    }

    virtual void from_json(rapidjson::Document& doc) override {
        using ex = pizza::config_exception;
        basic_config::from_json(doc);
        broker_addr_ = json::get<const char*>("broker_addr", doc);
        if (auto balancer_addr = json::maybe_get<const char*>("balancer_addr", doc); balancer_addr) {
            balancer_addr_ = std::move(*balancer_addr);
            has_balancer_ = true;
        }
        if (auto bus_pubs_addr = json::maybe_get<const char*>("bus_publishers_addr", doc); bus_pubs_addr) {
            bus_publishers_addr_ = std::move(*bus_pubs_addr);
            bus_status_ |=  bus_status::has_publishers_addr;
        }
        if (auto bus_subs_addr = json::maybe_get<const char*>("bus_subscribers_addr", doc); bus_subs_addr) {
            bus_subscribers_addr_ = std::move(*bus_subs_addr);
            bus_status_ |= bus_status::has_subscribers_addr;
        }

        if (bus_status_ == has_publishers_addr || bus_status_ == has_subscribers_addr)
            throw ex(ex::code::missing_member, "check for bus_subscribers_addr and bus_publishers_addr");

        if (doc.HasMember("cl_args")) {
            if (!doc["cl_args"].IsArray())
                throw ex(ex::code::member_of_wrong_type, "'cl_args' must be array");
            for (auto& arg : doc["cl_args"].GetArray()) {
                if (!arg.IsString())
                    throw ex(ex::code::member_of_wrong_type, "'cl_args' elements must be string");
                cl_args_.emplace_back(arg.GetString());
            }
        }
        threads_ = json::maybe_get<std::size_t>("threads", doc).value_or(1);
    }

    DECLARE_KEY_GETTER(bus_sub_public_key);
    DECLARE_KEY_GETTER(bus_pub_public_key);
    DECLARE_KEY_GETTER(balancer_public_key);
    DECLARE_KEY_GETTER(broker_public_key);

private:
    std::string broker_addr_;
    std::string balancer_addr_;
    std::string bus_subscribers_addr_;
    std::string bus_publishers_addr_;
    std::vector<std::string> cl_args_;
    std::size_t threads_ = 1;

    bool has_balancer_ = false;
    int bus_status_ = bus_status::no_bus;

    static void add_cl_args(const std::vector<std::string>& cl_args, rapidjson::Document& doc) {
        rapidjson::Value k, v;
        k.SetString("cl_args");
        v.SetArray();
        for (const auto& arg : cl_args) {
            rapidjson::Value elem;
            elem.SetString(arg.data(), arg.size());
            v.GetArray().PushBack(std::move(elem), doc.GetAllocator());
        }
        doc.AddMember(std::move(k), std::move(v), doc.GetAllocator());
    }
};

}
