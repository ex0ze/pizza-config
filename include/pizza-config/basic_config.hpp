/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <unordered_map>
#include <string>
#include <string_view>

#include "config_exception.hpp"
#include "json_helpers.hpp"

#define DECLARE_KEY_GETTER(key_name)                 \
    std::optional<std::string> key_name() const {    \
        if (auto iter = keys().find(#key_name);      \
            iter != keys().end())                    \
            return iter->second;                     \
        return std::nullopt;                         \
    }                                                \

namespace pizza
{

class basic_config {
public:
    enum class application {
        unspecified,
        balancer,
        broker,
        bus,
        service,
        requester
    };

    static application application_from_json(rapidjson::Document& doc) {
        if (doc.HasMember("app") && doc["app"].IsString()) {
            std::string_view str_app(doc["app"].GetString(), doc["app"].GetStringLength());
            return app_from_string(str_app);
        }
        return application::unspecified;
    }

    using key_map = std::unordered_map<std::string, std::string>;
    virtual ~basic_config() = default;

    const std::string& type() const noexcept { return type_; }
    void set_type(std::string type) noexcept { type_ = std::move(type); }

    constexpr application app() const noexcept { return app_; }
    constexpr void set_app(application app) noexcept { app_ = app; }

    const std::string& name() const noexcept { return name_; }
    void set_name(std::string name) noexcept { name_ = std::move(name); }

    const std::string& executable() const noexcept { return executable_; }
    void set_executable(std::string executable) noexcept {
        has_executable_ = true;
        executable_ = std::move(executable);
    }
    constexpr bool has_executable() const noexcept { return has_executable_; }

    constexpr bool encryption() const noexcept { return encryption_; }
    constexpr void set_encryption(bool encryption) noexcept { encryption_ = encryption; }

    constexpr const key_map& keys() const noexcept { return keys_; }
    constexpr key_map& keys() noexcept { return keys_; }

    virtual void to_json(rapidjson::Document& doc) const {
        json::add("type", type_.c_str(), doc, doc.GetAllocator());
        json::add("app", app_to_string(app_).data(), doc, doc.GetAllocator());
        json::add("name", name_.c_str(), doc, doc.GetAllocator());
        if (has_executable_)
            json::add("executable", executable_.c_str(), doc, doc.GetAllocator());
        if (encryption_) {
            json::add("encryption", encryption_, doc, doc.GetAllocator());
            json::add_keys(keys_, doc);
        }
    }

    virtual void from_json(rapidjson::Document& doc) {
        using ex = config_exception;
        type_ = json::get<const char*>("type", doc);
        const char * _app = json::get<const char*>("app", doc);
        app_ = app_from_string(_app);
        if (app_ == application::unspecified) throw ex(ex::code::undefined_app, _app);
        name_ = json::get<const char*>("name", doc);
        if (auto e = json::maybe_get<const char*>("executable", doc); e) {
            executable_ = *e;
            has_executable_ = true;
        }
        else {
            has_executable_ = false;
        }
        if (doc.HasMember("encryption")) {
            encryption_ = json::get<bool>("encryption", doc);
            get_keys(doc);
        }
    }

private:
    std::string type_;
    application app_ = application::unspecified;
    std::string name_;
    std::string executable_;
    bool has_executable_ = false;
    bool encryption_ = false;
    key_map keys_;

    static std::string_view app_to_string(application app) {
        switch (app) {
            case application::balancer: return "balancer";
            case application::broker: return "broker";
            case application::bus: return "bus";
            case application::service: return "service";
            case application::requester: return "requester";
            default: throw std::runtime_error(__func__);
        }
    }
    static application app_from_string(std::string_view str) noexcept {
        if (str == "balancer")       return application::balancer;
        else if (str == "broker")    return application::broker;
        else if (str == "bus")       return application::bus;
        else if (str == "service")   return application::service;
        else if (str == "requester") return application::requester;
        else                         return application::unspecified;
    }

    void get_keys(rapidjson::Document& doc) {
        if (!doc.HasMember("keys"))
            throw json::ex(json::ex::code::missing_member, "keys");
        if (!doc["keys"].IsObject())
            throw json::ex(json::ex::code::member_of_wrong_type, "'keys' must be object");
        auto key_obj = doc["keys"].GetObject();
        for (auto it = key_obj.MemberBegin(); it != key_obj.MemberEnd(); ++it) {
            if (!it->value.IsString())
                throw json::ex(json::ex::code::member_of_wrong_type, std::string(it->name.GetString()) + " must be string type");
            keys_.emplace(it->name.GetString(), it->value.GetString());
        }
    }
};

}
