/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "basic_config.hpp"

namespace pizza
{

class bus : public basic_config {
public:
    const std::string& publishers_addr() const noexcept { return publishers_addr_; }
    void set_publishers_addr(std::string publishers_addr) { publishers_addr_ = std::move(publishers_addr); }

    const std::string& subscribers_addr() const noexcept { return subscribers_addr_; }
    void set_subscribers_addr(std::string subscribers_addr) { subscribers_addr_ = std::move(subscribers_addr); }

    virtual void to_json(rapidjson::Document& doc) const override {
        basic_config::to_json(doc);
        json::add("publishers_addr", publishers_addr_.c_str(), doc, doc.GetAllocator());
        json::add("subscribers_addr", subscribers_addr_.c_str(), doc, doc.GetAllocator());
    }

    virtual void from_json(rapidjson::Document& doc) override {
        basic_config::from_json(doc);
        publishers_addr_ = json::get<const char*>("publishers_addr", doc);
        subscribers_addr_ = json::get<const char*>("subscribers_addr", doc);
    }

    DECLARE_KEY_GETTER(bus_sub_private_key);
    DECLARE_KEY_GETTER(bus_sub_public_key);
    DECLARE_KEY_GETTER(bus_pub_private_key);
    DECLARE_KEY_GETTER(bus_pub_public_key);

private:
    std::string publishers_addr_;
    std::string subscribers_addr_;
};

}
