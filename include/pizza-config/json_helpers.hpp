/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <optional>
#include <string>
#include <string_view>
#include <vector>
#include <unordered_map>

#include <rapidjson/document.h>

#include "config_exception.hpp"

namespace json
{
using ex = pizza::config_exception;
using key_map = std::unordered_map<std::string, std::string>;

template <typename Val, typename Alloc>
static void add(std::string_view key, const Val& value, rapidjson::Value& object, Alloc& allocator) {
    rapidjson::Value k, v;
    k.SetString(key.data(), key.size());
    v.template Set<Val>(value);
    object.AddMember(std::move(k), std::move(v), allocator);
}

static void add_keys(const key_map& keys, rapidjson::Document& doc) {
    rapidjson::Value k, v;
    k.SetString("keys");
    v.SetObject();
    for (auto [description, key] : keys) {
        rapidjson::Value key_desc, key_value;
        key_desc.SetString(description.c_str(), doc.GetAllocator());
        key_value.SetString(key.c_str(), doc.GetAllocator());
        v.AddMember(std::move(key_desc), std::move(key_value), doc.GetAllocator());
    }
    doc.AddMember(std::move(k), std::move(v), doc.GetAllocator());
}

template <typename T, typename Json>
static T get(const char * key, Json& doc) {
    if (!doc.HasMember(key)) throw ex(ex::code::missing_member, key);
    if (!doc[key].template Is<T>())  throw ex(ex::code::member_of_wrong_type, key);
    return doc[key].template Get<T>();
}

template <typename T, typename Json>
static std::optional<T> maybe_get(const char * key, Json& doc) {
    if (!doc.HasMember(key)) return std::nullopt;
    if (!doc[key].template Is<T>()) throw ex(ex::code::member_of_wrong_type, key);
    return doc[key].template Get<T>();
}

}
