/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <exception>
#include <sstream>
#include <string>

namespace pizza
{

class config_exception : public std::exception {
public:
    enum class code {
        unspecified,
        undefined_format,
        missing_member,
        member_of_wrong_type,
        undefined_app
    };
    config_exception(code c, std::string what) noexcept : what_(std::move(what)), c_(c) {}
    virtual const char * what() const noexcept override {
        if (!buf_.empty()) return buf_.c_str();
        std::ostringstream stream;
        stream << code_to_string() << ": " << what_;
        buf_ = stream.str();
        return buf_.c_str();
    }
private:
    std::string what_;
    code c_;
    mutable std::string buf_;

    constexpr const char * code_to_string() const noexcept {
        switch (c_) {
            case code::undefined_format: return "undefined file format";
            case code::missing_member: return "missing member";
            case code::member_of_wrong_type: return "member of wrong type";
            case code::undefined_app: return "undefined application";
            case code::unspecified: default: return "unknown error";
        }
    }
};

}
