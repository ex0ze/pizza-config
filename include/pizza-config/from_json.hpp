/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <filesystem>
#include <fstream>

#include "balancer.hpp"
#include "broker.hpp"
#include "bus.hpp"
#include "parse.hpp"
#include "requester.hpp"
#include "service.hpp"

namespace pizza
{

namespace detail
{

template <typename Application>
struct enum_for_application {};

#define DECLARE_ENUM_FOR_APPLICATION(V)                \
template<> struct enum_for_application<V> {            \
    static constexpr basic_config::application value = \
        basic_config::application::V;                  \
}                                                      \

DECLARE_ENUM_FOR_APPLICATION(balancer);
DECLARE_ENUM_FOR_APPLICATION(broker);
DECLARE_ENUM_FOR_APPLICATION(bus);
DECLARE_ENUM_FOR_APPLICATION(requester);
DECLARE_ENUM_FOR_APPLICATION(service);

}

template <typename Application>
static Application from_json(const std::string& path) {
    if (!std::filesystem::exists(path))
        throw std::runtime_error(path + " does not exist");
    std::ifstream in(path);
    in.seekg(0, std::ios::end);
    std::size_t size = in.tellg();
    in.seekg(0, std::ios::beg);
    std::string buf;
    buf.resize(size);
    in.read(buf.data(), buf.size());
    in.close();
    auto result = parse(buf);
    if (result->app() != detail::enum_for_application<Application>().value)
        throw std::runtime_error("Wrong type of application");
    return *dynamic_cast<Application*>(result.get());
}

template <typename Application>
static Application from_stdin() {
    auto flags = std::cin.flags();
    std::cin >> std::noskipws;
    std::istream_iterator<char> begin(std::cin), end;
    std::string buf(begin, end);
    std::cin.setf(flags);

    auto result = parse(buf);
    if (result->app() != detail::enum_for_application<Application>().value)
        throw std::runtime_error("Wrong type of application");
    return *dynamic_cast<Application*>(result.get());
}

}
