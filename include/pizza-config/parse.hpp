/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>

#include "balancer.hpp"
#include "broker.hpp"
#include "bus.hpp"
#include "requester.hpp"
#include "service.hpp"

#include <rapidjson/error/en.h>


namespace pizza
{

static std::unique_ptr<basic_config> parse(const std::string& str) {
    rapidjson::Document doc;
    rapidjson::ParseResult result = doc.Parse(str.data(), str.size());
    if (!result) {
        std::stringstream stream;
        stream << "failed parsing json: " << rapidjson::GetParseError_En(result.Code()) << " (" << result.Offset() << ")";
        throw std::runtime_error(stream.str());
    }
    using app = basic_config::application;
    auto app_ = basic_config::application_from_json(doc);
    if (app_ == basic_config::application::unspecified)
        throw config_exception(config_exception::code::undefined_app, "");
    switch (app_) {
        case app::balancer: {
            auto res = std::make_unique<balancer>();
            res->from_json(doc);
            return res;
        }
        case app::broker: {
            auto res = std::make_unique<broker>();
            res->from_json(doc);
            return res;
        }
        case app::bus: {
            auto res = std::make_unique<bus>();
            res->from_json(doc);
            return res;
        }
        case app::service: {
            auto res = std::make_unique<service>();
            res->from_json(doc);
            return res;
        }
        case app::requester: {
            auto res = std::make_unique<requester>();
            res->from_json(doc);
            return res;
        }
        default: throw std::runtime_error(__func__);
    }
}

}
